=== Advanced Custom Fields: Vimeo Upload Field ===
Contributors: AUTHOR_NAME
Tags: PLUGIN_TAGS
Requires at least: 3.5
Tested up to: 4.8.4
Stable tag: 2.0.0
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

SHORT_DESCRIPTION

== Description ==

EXTENDED_DESCRIPTION

= Compatibility =

This ACF field type is compatible with:
* ACF 5
* ACF 4

== Installation ==

1. Copy the `acf-vimeo_upload` folder into your `wp-content/plugins` folder
2. Activate the Vimeo Upload plugin via the plugins admin page
3. Create a new field via ACF and select the Vimeo Upload type
4. Please refer to the description for more info regarding the field type settings

== Changelog ==

= 1.0.0 =
* Initial Release.