# Changelog

## 2.0.0 - 2017-09-23

- Updated Vimeo and added ACF 5 support. Removed ACF 3.

## 1.0.10 - 2016-03-23

- Fixed format_value

## 1.0.9 - 2016-03-23

- Fixed field js in flexis and repeaters.

## 1.0.8 - 2014-07-19

- Added support for global API variables

## 1.0.6 - 2014-06-02

- Fixed some delete issues

## 1.0.5 - 2014-05-22

- Deleted the old v3 file
- Some cleanup

## 1.0.3 - 2014-04-22

- Fixed delete problems
- Fixed post_id problem when no global $post object is available
- Fixed uploads to wrong post when form is updating user or taxonomy, now it is using the same post_id as the form

## 1.0.0 - 2013

- Initial release
