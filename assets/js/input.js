(function($){


	function initialize_field( $el ) {

		//$el.doStuff();

	}


	if ( typeof acf.add_action !== 'undefined' ) {

		/*
		*  ready append (ACF5)
		*
		*  These are 2 events which are fired during the page load
		*  ready = on page load similar to $(document).ready()
		*  append = on new DOM elements appended via repeater field
		*
		*  @type	event
		*  @date	20/07/13
		*
		*  @param	$el (jQuery selection) the jQuery element which contains the ACF fields
		*  @return	n/a
		*/

		acf.add_action('ready append', function( $el ){

			set_handlers();

		});

	} else {


		/*
		*  acf/setup_fields (ACF4)
		*
		*  This event is triggered when ACF adds any new elements to the DOM.
		*
		*  @type	function
		*  @since	1.0.0
		*  @date	01/01/12
		*
		*  @param	event		e: an event object. This can be ignored
		*  @param	Element		postbox: An element which contains the new HTML
		*
		*  @return	n/a
		*/

		$(document).on('acf/setup_fields', function(e, postbox){

			set_handlers();

		});
	}

	function set_handlers() {

		postbox = $('body');

		$('.acf_vimeo_delete a').on('click', function(e){
			e.preventDefault();

			var $this = $(this),
				$spinner = $('<img />', {'class': "acf_vimeo_spinner", 'src': acf_vimeo.multipart_params.spinner, 'title': "Loading..."});

			$this.after( $spinner );

			$.ajax({
				url:      acf_vimeo.url,
				type:     'post',
				async:    true,
				cache:    false,
				dataType: 'html',
				data: {
					action:   'acf_vimeo_delete',
					post_id:  acf.o.post_id,//acf_vimeo.multipart_params.post_id,
					key:      $this.data('key'),
					nonce:    $this.data('nonce')
				},

				success: function( response )
				{
					$spinner.remove();

					//console.log(response);
					if ( '1' === response )
					{
						var $video_container  = $this.closest('._container'),
							$upload_container = $('.acf_vimeo_upload', $video_container),
							$field_container  = $this.closest('#acf-upload_video');

						// Empty the input, so it doesn't come back
						$('input[data-key="'+$this.data('key')+'"]').val('');

						// Physically remove items from page
						$upload_container.fadeOut(400, function(){
							$(this).removeClass('acf_vimeo_upload_show');
							$('.acf_vimeo_video, p.vimeo_upload_success', $upload_container).remove();
						});
						$('.vimeo_uploader_browse', $video_container).removeClass('vimeo_uploader_button_hide').fadeIn();
					}
				},

				error: function( xhr )
				{
					console.log(xhr.responseText);
				}
			});
		});

		$(postbox).find('.acf-field-vimeo-upload').each(function(){

			var $this      = $(this),
				key        = $this.data('key'),
				this_id    = $('.acf-input > input', this).attr('id'),//'acf-' + key,
				$form      = $this.closest('form'),
				this_vimeo = $.extend({},acf_vimeo);

			this_vimeo.browse_button  = this_id + this_vimeo.browse_button;
			this_vimeo.container      = this_id + this_vimeo.container;
			this_vimeo.drop_element   = this_id + this_vimeo.drop_element;
			this_vimeo.file_data_name = key + this_vimeo.file_data_name;

			this_vimeo.multipart_params.key     = key;
			this_vimeo.multipart_params.post_id = acf.o.post_id;

			var container_id = this_vimeo.container;
			var $container = $('#' + container_id);
			var $filelist = $container.find('.acf_vimeo_filelist');

			var uploader = new plupload.Uploader(this_vimeo);
			uploader.init();

			// Add file to the queue and for visual feedback
			uploader.bind('FilesAdded', function(up, files) {

				$('button, input[type="submit"], input[type="button"]', $form).prop('disabled', true).addClass('disabled').css({opacity: '0.6'});

				// Hide the upload button
				$('.vimeo_uploader_browse', $container).fadeOut('400', function(){ $(this).addClass('vimeo_uploader_button_hide'); });

				$.each(files, function(i, file) {
					$filelist.append(
						'<div class="file" id="' + file.id + '">' +
						'<strong>' + file.name + '</strong>' +
						' (<span>' + plupload.formatSize(0) + '</span>/' + plupload.formatSize(file.size) + ') ' +
						'<div class="fileprogress"></div>' +
						'</div>'
					);
				});

				up.refresh();
				up.start();
			});

			// Update the progress bar for the uploaded file in the queue
			uploader.bind('UploadProgress', function(up, file) {
				$('#' + file.id + " .fileprogress").width(file.percent + "%");
				$('#' + file.id + " span").html( plupload.formatSize(parseInt(file.size * file.percent / 100)) );

				if ( 100 == file.percent && ! $('.acf_vimeo_spinner').length )
				{
					$filelist.append('<div class="acf_vimeo_spinner"><img src="' + this_vimeo.multipart_params.spinner + '" /> Updating, Don\'t Refresh</div>');
				}
			});

			// Finished uploading
			uploader.bind('FileUploaded', function(up, file, response) {
				$('button, input[type="submit"], input[type="button"]', $form).prop('disabled', false).removeClass('disabled').css({opacity: '1'});

				//console.log(response);
				if ( response.status )
				{
					response = $.parseJSON(response.response);
					//console.log(response);

					$('.acf_vimeo_spinner, #' + file.id).fadeOut();

					// Add the video id to input
					$('#' + this_id).val(response.video_id);

					// Add the video thumbnail to the page
					var $upload_container = $('.acf_vimeo_upload', $container);
					$upload_container.fadeIn(400, function(){ $(this).addClass('acf_vimeo_upload_show'); }).prepend(response.embed).prepend('<p class="vimeo_upload_success"><strong>Success!</strong></p>');

					$('#' + file.id).remove();
				}
			});

		});

	} // end set_handlers()

})(jQuery);
