<?php

// exit if accessed directly
if( ! defined( 'ABSPATH' ) ) exit;


// check if class already exists
if( !class_exists('acf_field_vimeo_uploader') ) :

require_once( 'vimeo_uploader_common.php' );

class acf_field_vimeo_uploader extends acf_field {
	use vimeo_uploader_common;

	// vars
	var $settings, // will hold info such as dir / path
		$defaults; // will hold default field options

	/*
	*  __construct
	*
	*  Set name / label needed for actions / filters
	*
	*  @since	3.6
	*  @date	23/01/13
	*/

	function __construct( $settings )
	{
		// vars
		$this->name = 'vimeo_uploader';
		$this->label = __('Vimeo Uploader');
		$this->category = __("Content",'acf'); // Basic, Content, Choice, etc
		$this->defaults = array(
			'client_id' => '',
			'client_secret' => '',
			'access_token' => '',
			'access_secret' => ''
		);

		// do not delete!
    	parent::__construct();


    	// settings
	$this->settings = $settings;
	/*
	$this->settings = array(
		'path' => apply_filters('acf/helpers/get_path', __FILE__),
		'dir' => apply_filters('acf/helpers/get_dir', __FILE__),
		'version' => '2.0.0',
		'nonce_upload' => 'acf_vimeo_uploader_upload',
		'nonce_delete' => 'acf_vimeo_uploader_delete',
		'embed_url' => 'player.vimeo.com/video/'
	);
	*/
	$this->set_actions();

	}


	/*
	*  create_options()
	*
	*  Create extra options for your field. This is rendered when editing a field.
	*  The value of $field['name'] can be used (like below) to save extra data to the $field
	*
	*  @type	action
	*  @since	3.6
	*  @date	23/01/13
	*
	*  @param	$field	- an array holding all the field's data
	*/

	function create_options( $field )
	{
		// defaults?
		/*
		$field = array_merge($this->defaults, $field);
		*/

		// key is needed in the field names to correctly save the data
		$key = $field['name'];


		// Create Field Options HTML
		?>
		<tr class="field_option field_option_<?php echo $this->name; ?>">
			<td class="label">
				<label><?php _e("Client ID",'acf'); ?></label>
				<p class="description"><?php _e("(Also known as Consumer Key or API Key)",'acf'); ?></p>
			</td>
			<td>
				<?php

				do_action('acf/create_field', array(
					'type'		=>	'text',
					'name'		=>	'fields['.$key.'][client_id]',
					'value'		=>	$field['client_id'],
					'layout'	=>	'horizontal'
				));

				?>
			</td>
		</tr>
		<tr class="field_option field_option_<?php echo $this->name; ?>">
			<td class="label">
				<label><?php _e("Client Secret",'acf'); ?></label>
				<p class="description"><?php _e("(Also known as Consumer Secret or API Secret)",'acf'); ?></p>
			</td>
			<td>
				<?php

				do_action('acf/create_field', array(
					'type'		=>	'text',
					'name'		=>	'fields['.$key.'][client_secret]',
					'value'		=>	$field['client_secret'],
					'layout'	=>	'horizontal'
				));

				?>
			</td>
		</tr>
		<tr class="field_option field_option_<?php echo $this->name; ?>">
			<td class="label">
				<label><?php _e("Access token",'acf'); ?></label>
				<p class="description"><?php _e("Your OAuth Access Token to access your account with this app.",'acf'); ?></p>
			</td>
			<td>
				<?php

				do_action('acf/create_field', array(
					'type'		=>	'text',
					'name'		=>	'fields['.$key.'][access_token]',
					'value'		=>	$field['access_token'],
					'layout'	=>	'horizontal'
				));

				?>
			</td>
		</tr>
		<?php

	}


	/*
	*  create_field()
	*
	*  Create the HTML interface for your field
	*
	*  @param	$field - an array holding all the field's data
	*
	*  @type	action
	*  @since	3.6
	*  @date	23/01/13
	*/

	function create_field( $field )
	{
		$video_id = $field['value'];
		?>
		<input type="hidden" id="<?php esc_attr_e( $field['id'] ); ?>" class="<?php esc_attr_e( $field['class'] ); ?>" name="<?php esc_attr_e( $field['name'] ); ?>" data-key="<?php esc_attr_e( $field['key'] ); ?>" value="<?php esc_attr_e( $field['value'] ); ?>" />

		<div id="<?php esc_attr_e( $field['id'] ); ?>_container" class="<?php esc_attr_e( $field['class'] ); ?>_container">
			<input type="button" id="<?php esc_attr_e( $field['id'] ); ?>_browse" class="button <?php esc_attr_e( $field['class'] ); ?>_browse<?php if ( $video_id ) echo ' '. esc_attr( $field['class'] ) .'_button_hide'; ?>" value="<?php esc_attr_e('Select File'); ?>" />
			<div class="acf_vimeo_filelist"></div>

			<div class="acf_vimeo_upload<?php if ( $video_id ) echo " acf_vimeo_upload_show"; ?>">
				<?php if ( $video_id ) echo $this->vimeo_embed($video_id); ?>
				<div class="acf_vimeo_delete small<?php if ( $video_id ) echo " acf_vimeo_delete_show"; ?>"><a href="#delete" data-key="<?php esc_attr_e( $field['key'] ); ?>" data-nonce="<?php echo wp_create_nonce($this->settings['nonce_delete'] . $field['key']); ?>" title="Delete Video Upload">Delete</a></div>
			</div>
		</div>
		<?php
	}


	/*
	*  input_admin_enqueue_scripts()
	*
	*  This action is called in the admin_enqueue_scripts action on the edit screen where your field is created.
	*  Use this action to add CSS + JavaScript to assist your create_field() action.
	*
	*  $info	http://codex.wordpress.org/Plugin_API/Action_Reference/admin_enqueue_scripts
	*  @type	action
	*  @since	3.6
	*  @date	23/01/13
	* /

	function input_admin_enqueue_scripts()
	{

		// vars
		$url = $this->settings['url'];
		$version = $this->settings['version'];


		// register & include JS
		wp_register_script( 'acf-input-vimeo_upload', "{$url}assets/js/input.js", array('acf-input'), $version );
		$args = array(
			'url'                  => admin_url( 'admin-ajax.php' ),
			'runtimes'             => 'html5,silverlight,flash,html4',
			'browse_button'        => '_browse',
			'container'            => '_container',
			'drop_element'         => '_drop',
			'file_data_name'       => '_file',
			'multiple_queues'      => true,
			'max_file_size'        => wp_max_upload_size() . 'b',
			'flash_swf_url'        => includes_url('js/plupload/plupload.flash.swf'),
			'silverlight_xap_url'  => includes_url('js/plupload/plupload.silverlight.xap'),
			'filters'              => array(array('title' => __('Allowed Files'), 'extensions' => '*')),
			'multipart'            => true,
			'urlstream_upload'     => true,
			'multi_selection'      => false,
			// additional post data to send to our ajax hook
			'multipart_params'     => array(
				#'file_id'              => '', // will be added per uploader
				'action'               => 'acf_vimeo_upload',
				'nonce'                => wp_create_nonce( $this->settings['nonce_upload'] ),
				'post_id'              => ( isset($GLOBALS['post']) && is_object($GLOBALS['post']) ? $GLOBALS['post']->ID : 0),
				'key'                  => '',
				'spinner'              => admin_url( 'images/loading.gif' )
			)
		);

		wp_localize_script( 'acf-input-vimeo_upload', 'acf_vimeo', $args );

		wp_enqueue_script( array(
			'jQuery',
			'plupload',
			'plupload-all',
			'acf-input-vimeo_upload'
		));


		// register & include CSS
		wp_register_style( 'acf-input-vimeo_upload', "{$url}../assets/css/input.css", array('acf-input'), $version );
		wp_enqueue_style('acf-input-vimeo_upload');

	}


	/*
	*  format_value_for_api()
	*
	*  This filter is applied to the $value after it is loaded from the db and before it is passed back to the API functions such as the_field
	*
	*  @type	filter
	*  @since	3.6
	*  @date	23/01/13
	*
	*  @param	$value	- the value which was loaded from the database
	*  @param	$post_id - the $post_id from which the value was loaded
	*  @param	$field	- the field array holding all the field options
	*
	*  @return	$value	- the modified value
	*/

	function format_value_for_api( $value, $post_id, $field )
	{
		if ( $value )
		{
			$value = $this->vimeo_embed($value);
		}
		return $value;
	}

}


// initialize
new acf_field_vimeo_uploader( $this->settings );


// class_exists check
endif;

?>
