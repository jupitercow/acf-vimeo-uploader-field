<?php

// exit if accessed directly
if( ! defined( 'ABSPATH' ) ) exit;


// check if class already exists
if( !class_exists('acf_field_vimeo_upload') ) :

require_once( 'vimeo_uploader_common.php' );


class acf_field_vimeo_upload extends acf_field {
	use vimeo_uploader_common;
	var $settings, // will hold info such as dir / path
		$defaults; // will hold default field options

	/*
	*  __construct
	*
	*  This function will setup the field type data
	*
	*  @type	function
	*  @date	5/03/2014
	*  @since	5.0.0
	*
	*  @param	n/a
	*  @return	n/a
	*/

	function __construct( $settings ) {

		/*
		*  name (string) Single word, no spaces. Underscores allowed
		*/

		$this->name = 'vimeo_upload';


		/*
		*  label (string) Multiple words, can include spaces, visible when selecting a field type
		*/

		$this->label = __('Vimeo Upload', 'acf-vimeo_upload');


		/*
		*  category (string) basic | content | choice | relational | jquery | layout | CUSTOM GROUP NAME
		*/

		$this->category = 'basic';


		/*
		*  defaults (array) Array of default settings which are merged into the field object. These are used later in settings
		*/

		$this->defaults = array(
			'client_id' => '',
			'client_secret' => '',
			'access_token' => '',
			'access_secret' => ''
		);


		/*
		*  l10n (array) Array of strings that are used in JavaScript. This allows JS strings to be translated in PHP and loaded via:
		*  var message = acf._e('vimeo_upload', 'error');
		*/

		$this->l10n = array(
			'error'	=> __('Error! Please enter a higher value', 'acf-vimeo_upload'),
		);


		/*
		*  settings (array) Store plugin settings (url, path, version) as a reference for later use with assets
		*/

		// do not delete!
    	parent::__construct();

		$this->settings = $settings;
		$this->set_actions();

	}


	/*
	*  render_field_settings()
	*
	*  Create extra settings for your field. These are visible when editing a field
	*
	*  @type	action
	*  @since	3.6
	*  @date	23/01/13
	*
	*  @param	$field (array) the $field being edited
	*  @return	n/a
	*/

	function render_field_settings( $field ) {

		/*
		*  acf_render_field_setting
		*
		*  This function will create a setting for your field. Simply pass the $field parameter and an array of field settings.
		*  The array of settings does not require a `value` or `prefix`; These settings are found from the $field array.
		*
		*  More than one setting can be added by copy/paste the above code.
		*  Please note that you must also have a matching $defaults value for the field name (font_size)
		* /

		acf_render_field_setting( $field, array(
			'label'			=> __('Font Size','acf-vimeo_upload'),
			'instructions'	=> __('Customise the input font size','acf-vimeo_upload'),
			'type'			=> 'number',
			'name'			=> 'font_size',
			'prepend'		=> 'px',
		));
		/**/
		acf_render_field_setting( $field, array(
			'label'			=> __('Client ID','acf-vimeo_upload'),
			'instructions'	=> __('(Also known as Consumer Key or API Key)','acf-vimeo_upload'),
			'type'			=> 'text',
			'name'			=> 'client_id',
		));
		acf_render_field_setting( $field, array(
			'label'			=> __('Client Secret','acf-vimeo_upload'),
			'instructions'	=> __('(Also known as Consumer Secret or API Secret)','acf-vimeo_upload'),
			'type'			=> 'text',
			'name'			=> 'client_secret',
		));
		acf_render_field_setting( $field, array(
			'label'			=> __('Access token','acf-vimeo_upload'),
			'instructions'	=> __('Your OAuth Access Token to access your account with this app.','acf-vimeo_upload'),
			'type'			=> 'text',
			'name'			=> 'access_token',
		));
	}



	/*
	*  render_field()
	*
	*  Create the HTML interface for your field
	*
	*  @param	$field (array) the $field being rendered
	*
	*  @type	action
	*  @since	3.6
	*  @date	23/01/13
	*
	*  @param	$field (array) the $field being edited
	*  @return	n/a
	*/

	function render_field( $field ) {

		$video_id = $field['value'];
		?>
		<input type="hidden" id="<?php esc_attr_e( $field['id'] ); ?>" class="<?php esc_attr_e( $field['class'] ); ?>" name="<?php esc_attr_e( $field['name'] ); ?>" data-key="<?php esc_attr_e( $field['key'] ); ?>" value="<?php esc_attr_e( $field['value'] ); ?>" />

		<div id="<?php esc_attr_e( $field['id'] ); ?>_container" class="<?php esc_attr_e( $field['class'] ); ?>_container">
			<input type="button" id="<?php esc_attr_e( $field['id'] ); ?>_browse" class="button <?php esc_attr_e( $field['class'] ); ?>_browse<?php if ( $video_id ) echo ' '. esc_attr( $field['class'] ) .'_button_hide'; ?>" value="<?php esc_attr_e('Select File'); ?>" />
			<div class="acf_vimeo_filelist"></div>

			<div class="acf_vimeo_upload<?php if ( $video_id ) echo " acf_vimeo_upload_show"; ?>">
				<?php
				if ( $video_id ) :
					echo $this->vimeo_embed($video_id); ?>
					<div class="acf_vimeo_delete small<?php if ( $video_id ) echo " acf_vimeo_delete_show"; ?>"><a href="#delete" data-key="<?php esc_attr_e( $field['key'] ); ?>" data-nonce="<?php echo wp_create_nonce($this->settings['nonce_delete'] . $field['key']); ?>" title="Delete Video Upload">Delete</a></div>
				<?php
				endif;
				?>
			</div>
		</div>
		<?php
	}


	/*
	*  input_admin_enqueue_scripts()
	*
	*  This action is called in the admin_enqueue_scripts action on the edit screen where your field is created.
	*  Use this action to add CSS + JavaScript to assist your render_field() action.
	*
	*  @type	action (admin_enqueue_scripts)
	*  @since	3.6
	*  @date	23/01/13
	*
	*  @param	n/a
	*  @return	n/a
	* /

	function input_admin_enqueue_scripts() {

		// vars
		$url = plugin_dir_url(dirname(__FILE__));
		$version = $this->settings['version'];

		// register & include JS
		wp_register_script( 'acf-input-vimeo_upload', "{$url}assets/js/input.js", array('acf-input'), $version );
		$args = array(
			'url'                  => admin_url( 'admin-ajax.php' ),
			'runtimes'             => 'html5,silverlight,flash,html4',
			'browse_button'        => '_browse',
			'container'            => '_container',
			'drop_element'         => '_drop',
			'file_data_name'       => '_file',
			'multiple_queues'      => true,
			'max_file_size'        => wp_max_upload_size() . 'b',
			'flash_swf_url'        => includes_url('js/plupload/plupload.flash.swf'),
			'silverlight_xap_url'  => includes_url('js/plupload/plupload.silverlight.xap'),
			'filters'              => array(array('title' => __('Allowed Files'), 'extensions' => '*')),
			'multipart'            => true,
			'urlstream_upload'     => true,
			'multi_selection'      => false,
			// additional post data to send to our ajax hook
			'multipart_params'     => array(
				#'file_id'              => '', // will be added per uploader
				'action'               => 'acf_vimeo_upload',
				'nonce'                => wp_create_nonce( $this->settings['nonce_upload'] ),
				'post_id'              => ( isset($GLOBALS['post']) && is_object($GLOBALS['post']) ? $GLOBALS['post']->ID : 0),
				'key'                  => '',
				'spinner'              => admin_url( 'images/loading.gif' )
			)
		);
		wp_localize_script( 'acf-input-vimeo_upload', 'acf_vimeo', $args );

		wp_enqueue_script( array(
			'jQuery',
			'plupload',
			'plupload-all',
			'acf-input-vimeo_upload'
		));


		// register & include CSS
		wp_register_style( 'acf-input-vimeo_upload', "{$url}assets/css/input.css", array('acf-input'), $version );
		wp_enqueue_style('acf-input-vimeo_upload');
	}


	/*
	*  input_admin_enqueue_scripts()
	*
	*  This action is called in the admin_enqueue_scripts action on the edit screen where your field is created.
	*  Use this action to add CSS + JavaScript to assist your create_field() action.
	*
	*  $info	http://codex.wordpress.org/Plugin_API/Action_Reference/admin_enqueue_scripts
	*  @type	action
	*  @since	3.6
	*  @date	23/01/13
	* /

	function input_admin_enqueue_scripts()
	{

		// vars
		$url = $this->settings['url'];
		$version = $this->settings['version'];


		// register & include JS
		wp_register_script( 'acf-input-vimeo_upload', "{$url}assets/js/input.js", array('acf-input'), $version );
		$args = array(
			'url'                  => admin_url( 'admin-ajax.php' ),
			'runtimes'             => 'html5,silverlight,flash,html4',
			'browse_button'        => '_browse',
			'container'            => '_container',
			'drop_element'         => '_drop',
			'file_data_name'       => '_file',
			'multiple_queues'      => true,
			'max_file_size'        => wp_max_upload_size() . 'b',
			'flash_swf_url'        => includes_url('js/plupload/plupload.flash.swf'),
			'silverlight_xap_url'  => includes_url('js/plupload/plupload.silverlight.xap'),
			'filters'              => array(array('title' => __('Allowed Files'), 'extensions' => '*')),
			'multipart'            => true,
			'urlstream_upload'     => true,
			'multi_selection'      => false,
			// additional post data to send to our ajax hook
			'multipart_params'     => array(
				#'file_id'              => '', // will be added per uploader
				'action'               => 'acf_vimeo_upload',
				'nonce'                => wp_create_nonce( $this->settings['nonce_upload'] ),
				'post_id'              => ( isset($GLOBALS['post']) && is_object($GLOBALS['post']) ? $GLOBALS['post']->ID : 0),
				'key'                  => '',
				'spinner'              => admin_url( 'images/loading.gif' )
			)
		);

		wp_localize_script( 'acf-input-vimeo_upload', 'acf_vimeo', $args );

		wp_enqueue_script( array(
			'jQuery',
			'plupload',
			'plupload-all',
			'acf-input-vimeo_upload'
		));


		// register & include CSS
		wp_register_style( 'acf-input-vimeo_upload', "{$url}assets/css/input.css", array('acf-input'), $version );
		wp_enqueue_style('acf-input-vimeo_upload');

	}




	/*
	*  format_value()
	*
	*  This filter is appied to the $value after it is loaded from the db and before it is returned to the template
	*
	*  @type	filter
	*  @since	3.6
	*  @date	23/01/13
	*
	*  @param	$value (mixed) the value which was loaded from the database
	*  @param	$post_id (mixed) the $post_id from which the value was loaded
	*  @param	$field (array) the field array holding all the field options
	*
	*  @return	$value (mixed) the modified value
	*/

	function format_value( $value, $post_id, $field ) {

		// bail early if no value
		if( empty($value) ) {

			return $value;

		}

		$value = $this->vimeo_embed($value);

		// return
		return $value;
	}

}


// initialize
new acf_field_vimeo_upload( $this->settings );


// class_exists check
endif;

?>
