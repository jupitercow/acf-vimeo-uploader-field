<?php

	trait vimeo_uploader_common {

		function set_actions() {
			add_action( 'post_edit_form_tag',       array($this, 'post_edit_form_tag') );
			add_action( 'wp_ajax_acf_vimeo_upload', array($this, 'ajax_upload') );
			add_action( 'wp_ajax_acf_vimeo_delete', array($this, 'ajax_delete') );
		}

		public function post_edit_form_tag()
		{
			echo ' enctype="multipart/form-data"';
		}

		/*
		*  input_admin_enqueue_scripts()
		*
		*  This action is called in the admin_enqueue_scripts action on the edit screen where your field is created.
		*  Use this action to add CSS + JavaScript to assist your create_field() action.
		*
		*  $info	http://codex.wordpress.org/Plugin_API/Action_Reference/admin_enqueue_scripts
		*  @type	action
		*  @since	3.6
		*  @date	23/01/13
		*/

		function input_admin_enqueue_scripts()
		{

			// vars
			$url = $this->settings['url'];
			$version = $this->settings['version'];


			// register & include JS
			wp_register_script( 'acf-input-vimeo_upload', "{$url}assets/js/input.js", array('acf-input'), $version );
			$args = array(
				'url'                  => admin_url( 'admin-ajax.php' ),
				'runtimes'             => 'html5,silverlight,flash,html4',
				'browse_button'        => '_browse',
				'container'            => '_container',
				'drop_element'         => '_drop',
				'file_data_name'       => '_file',
				'multiple_queues'      => true,
				'max_file_size'        => wp_max_upload_size() . 'b',
				'flash_swf_url'        => includes_url('js/plupload/plupload.flash.swf'),
				'silverlight_xap_url'  => includes_url('js/plupload/plupload.silverlight.xap'),
				'filters'              => array(array('title' => __('Allowed Files'), 'extensions' => '*')),
				'multipart'            => true,
				'urlstream_upload'     => true,
				'multi_selection'      => false,
				// additional post data to send to our ajax hook
				'multipart_params'     => array(
					#'file_id'              => '', // will be added per uploader
					'action'               => 'acf_vimeo_upload',
					'nonce'                => wp_create_nonce( $this->settings['nonce_upload'] ),
					'post_id'              => ( isset($GLOBALS['post']) && is_object($GLOBALS['post']) ? $GLOBALS['post']->ID : 0),
					'key'                  => '',
					'spinner'              => admin_url( 'images/loading.gif' )
				)
			);

			wp_localize_script( 'acf-input-vimeo_upload', 'acf_vimeo', $args );

			wp_enqueue_script( array(
				'jQuery',
				'plupload',
				'plupload-all',
				'acf-input-vimeo_upload'
			));


			// register & include CSS
			wp_register_style( 'acf-input-vimeo_upload', "{$url}assets/css/input.css", array('acf-input'), $version );
			wp_enqueue_style('acf-input-vimeo_upload');

		}

		public function ajax_delete()
		{
	   		// vars
			$options = array(
				'nonce' => '',
				'post_id' => 0,
				'key' => ''
			);
			$return = array('status' => 0);

			// load post options
			$options = array_merge($options, $_POST);

			// test options
			foreach ( $options as $key => $option )
			{
				if (! $option ) die('missing option: '. $key);#
			}

			// verify nonce
			if (! wp_verify_nonce($options['nonce'], $this->settings['nonce_delete'] . $options['key']) )
			{
				die('Are you sure you want to do that?');
			}

			update_field($options['key'], '', $options['post_id']);

			echo 1;
			die;
		}


		public function ajax_upload()
		{
			// vars
			$options = array(
				#'file_id' => '',
				'nonce' => '',
				'post_id' => 0,
				'key' => ''
			);
			$return = array('status' => 0);

			// load post options
			$options = array_merge($options, $_POST);

			// test options
			foreach ( $options as $key => $option )
			{
				if (! $option ) die(0);#die('missing option: '. $key);
			}

			// verify nonce
			if( ! wp_verify_nonce($options['nonce'], $this->settings['nonce_upload']) )
			{
				die(0);#die('Are you sure you want to do that?');
			}

			require_once( realpath(dirname(__FILE__)).'/../Vimeo/autoload.php' );

			$field = get_field_object($options['key']);
			if (! $field ) die(0);

			// Don't set default on the construct because the they will still live in the DB / Export of ACF and when you change them in they get change everywhere
			$defaults = array(
				'client_id'     => ( defined('ACF_VIMEO_UPLOADER_CLIENT_ID')     && ACF_VIMEO_UPLOADER_CLIENT_ID )     ? ACF_VIMEO_UPLOADER_CLIENT_ID     : '',
				'client_secret' => ( defined('ACF_VIMEO_UPLOADER_CLIENT_SECRET') && ACF_VIMEO_UPLOADER_CLIENT_SECRET ) ? ACF_VIMEO_UPLOADER_CLIENT_SECRET : '',
				'access_token'  => ( defined('ACF_VIMEO_UPLOADER_ACCESS_TOKEN')  && ACF_VIMEO_UPLOADER_ACCESS_TOKEN )  ? ACF_VIMEO_UPLOADER_ACCESS_TOKEN  : '',
			);

			$field['client_id']     = ( ! empty( $defaults['client_id'] ) )      ? $defaults['client_id']     : $field['client_id'];
			$field['client_secret'] = ( ! empty( $defaults['client_secret'] ) )  ? $defaults['client_secret'] : $field['client_secret'];
			$field['access_token']  = ( ! empty( $defaults['access_token'] ) )   ? $defaults['access_token']  : $field['access_token'];
#error_log( 'AUTH: ' . $field['client_id']. ' : ' . $field['client_secret']. ' : ' . $field['access_token']);

			$vimeo = new \Vimeo\Vimeo( $field['client_id'], $field['client_secret'], $field['access_token'] );

			try
			{
				$video_uri = $vimeo->upload($_FILES[$options['key'] . '_file']['tmp_name']);
				$video_id = filter_var($video_uri, FILTER_SANITIZE_NUMBER_INT);

				if( $video_id )
				{
					update_field($options['key'], $video_id, $options['post_id']);

					#error_log( '$video_uri: ' . $video_uri  );
					#error_log( '========================' );
					#error_log( 'PARAMS:' );
					#error_log( print_r(
					#	array(
					#		'name' => apply_filters( 'acf/fields/vimeo/filename', get_the_title($options['post_id']), $options['post_id'] ),
					#	)
					#	, true ) );
					// set the title of the video to the parent post title
					$result = $vimeo->request(
						$video_uri,
						array(
							'name' => apply_filters( 'acf/fields/vimeo/filename', get_the_title($options['post_id']), $options['post_id'] ),
						), 'PATCH'
					);

					$return = array(
						'status' => 1,
						'video_id' => $video_id,
						'embed' => $this->vimeo_embed($video_id)
					);
				}
				else
				{
					wp_die( "Video file did not exist!" );
				}
			}
			catch ( VimeoAPIException $e )
			{
				die( "Encountered an API error -- code {$e->getCode()} - {$e->getMessage()}" );
			}

			// return json
			echo json_encode($return);
			die;
		}

		function embed_video( $url )
		{
			$output = '<div class="acf_vimeo_video"><iframe src="' . esc_attr($url) . '" frameborder="0" webkitAllowFullScreen mozallowfullscreen allowFullScreen></iframe></div>';
			return $output;
		}

		function http()
		{
			return (is_ssl() ? 'https' : 'http') . '://';
		}

		function vimeo_url( $video_id )
		{
			return $this->http() . $this->settings['embed_url'] . $video_id;
		}

		function vimeo_embed( $video_id )
		{
			return $this->embed_video( $this->vimeo_url($video_id) );
		}






	} /* END TRAIT */

?>
